FROM rust:1.52 as builder

WORKDIR /usr/src/myapp
RUN rustup component add rustfmt

COPY . .
RUN cargo install --path .

FROM debian:buster-slim
WORKDIR /app
RUN apt update && apt install -y libpq5

COPY --from=builder /usr/local/cargo/bin/* /app/
COPY --from=builder /usr/src/myapp/start_backend.sh /app/start_backend.sh

CMD ["./start_backend.sh"]
