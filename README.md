# ZorPass backend
Backend services for ZorPass. There are 4 gRPC services:
- Account : Handles user accounts
- Authentication : Handles authentication tokens
- MasterKeys : Handles users' master key
- Registry : Handles user passwords

## Usage

```bash
git clone git@gitlab.com:zorpass/backend.git && cd backend
git submodule update --init
sudo docker-compose up
```

