fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::compile_protos("api/credentials_service.proto")?;
    tonic_build::compile_protos("api/account_service.proto")?;
    tonic_build::compile_protos("api/master_key_service.proto")?;
    tonic_build::compile_protos("api/password_registry_service.proto")?;
    Ok(())
}
