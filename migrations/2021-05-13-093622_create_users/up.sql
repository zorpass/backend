CREATE TABLE users (
  id SERIAL NOT NULL PRIMARY KEY,
  username VARCHAR(45) UNIQUE NOT NULL,
  salt BYTEA NOT NULL,
  enc_login_secrets BYTEA NOT NULL,
  enc_public_key BYTEA NOT NULL
)