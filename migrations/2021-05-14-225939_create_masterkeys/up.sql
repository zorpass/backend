CREATE TABLE masterkeys (
  id INT NOT NULL PRIMARY KEY,
  master_key BYTEA NOT NULL
)