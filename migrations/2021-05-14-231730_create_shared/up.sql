CREATE TABLE share_offers (
  id SERIAL NOT NULL PRIMARY KEY,
  user_id INT NOT NULL,
  share_offer BYTEA NOT NULL
)