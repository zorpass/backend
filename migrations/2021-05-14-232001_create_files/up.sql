CREATE TYPE file_state AS ENUM ('fresh', 'inactive', 'active');

CREATE TABLE files (
  id SERIAL NOT NULL PRIMARY KEY,
  file_data BYTEA NOT NULL,
  state file_state NOT NULL
);
