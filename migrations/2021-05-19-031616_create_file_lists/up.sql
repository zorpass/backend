CREATE TABLE file_lists (
  user_id INT NOT NULL PRIMARY KEY,
  file_list BYTEA NOT NULL,
  version_tag BIGINT NOT NULL
)