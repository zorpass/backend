use anyhow::Result;
use dashmap::DashMap;
use diesel::{self, prelude::*, RunQueryDsl};
use scram_rs::scram_auth::{ScramAuthServer, ScramPassword};
use scram_rs::scram_hashing::{ScramHashing, ScramSha256};
use sodiumoxide::{crypto::secretbox, randombytes::randombytes};
use tonic::metadata::MetadataValue;
use tonic::transport::Channel;

use crate::models::{NewUser, User};
use common::api::{
    master_keys::master_key_client::MasterKeyClient, master_keys::MasterKeyUpdateRequest,
    password_registry::password_registry_client::PasswordRegistryClient,
    password_registry::UpdateFileListRequest,
};
use common::config::Config;
use common::credentials::{CredentialsVerifier, UserId};
use common::crypto::{CryptoError, EncryptedData};
use common::db::{self, DbConn};
use common::schema;

#[derive(Debug)]
pub enum AccountError {
    InvalidUsername,
    UnavailableUsername,
    InternalError,
    InvalidOTP,
    DieselError(diesel::result::Error),
}
impl From<diesel::result::Error> for AccountError {
    fn from(e: diesel::result::Error) -> Self {
        AccountError::DieselError(e)
    }
}
impl Into<&str> for AccountError {
    fn into(self) -> &'static str {
        match self {
            AccountError::InvalidUsername => "invalid username",
            AccountError::UnavailableUsername => "username already take",
            AccountError::InternalError => "internal error",
            AccountError::InvalidOTP => "invalid OTP",
            AccountError::DieselError(_) => "diesel error",
        }
    }
}
impl From<CryptoError> for AccountError {
    fn from(_: CryptoError) -> Self {
        AccountError::InternalError
    }
}

pub struct AccountManager {
    db_pool: db::DbPool,
    master_key_client: MasterKeyClient<Channel>,
    registry_client: PasswordRegistryClient<Channel>,
    login_secrets_cache: DashMap<String, LoginSecrets>,
}

impl AccountManager {
    pub async fn new(db_pool: db::DbPool, config: &Config) -> Result<AccountManager> {
        log::info!("Connecting to masterkeys service");
        let master_key_url = config.masterkeys.as_http_url();
        let channel = tonic::transport::Channel::from_shared(master_key_url)?
            .connect()
            .await?;
        let master_key_client = MasterKeyClient::new(channel);

        log::info!("Connecting to registry service");
        let registry_url = config.registry.as_http_url();
        let channel = tonic::transport::Channel::from_shared(registry_url)?
            .connect()
            .await?;
        let registry_client = PasswordRegistryClient::new(channel);

        Ok(AccountManager {
            db_pool,
            master_key_client,
            registry_client,
            login_secrets_cache: DashMap::new(),
        })
    }

    pub fn get_salt(&self, query_username: &str) -> Result<Vec<u8>, AccountError> {
        use common::schema::users::dsl::*;

        log::debug!("Getting salt for user {}", query_username);

        let db_conn = self.get_db_conn()?;

        let user_salt = users
            .filter(username.eq(query_username))
            .select(salt)
            .get_result::<Vec<u8>>(&db_conn)
            .map_err(|e| {
                log::debug!("Unable to load user {}: {}", query_username, e);
                AccountError::DieselError(e)
            })?;

        log::debug!("Salt for user {} is {:?}", query_username, user_salt);

        Ok(user_salt)
    }

    pub fn get_user_id(&self, query_username: &str) -> Result<UserId, AccountError> {
        use common::schema::users::dsl::*;

        log::debug!("Getting id for user {}", query_username);

        let db_conn = self.get_db_conn()?;
        let user_result = users
            .filter(username.eq(query_username))
            .get_result::<User>(&db_conn)
            .optional()
            .map_err(|e| {
                log::debug!("Unable to load user {}: {}", query_username, e);
                AccountError::InternalError
            })?;

        if let Some(user) = user_result {
            Ok(user.id)
        } else {
            log::debug!("No user found");
            Err(AccountError::UnavailableUsername)
        }
    }

    pub async fn pre_auth(
        &self,
        username: String,
        login_secrets_key: &[u8],
        user_otp: u32,
    ) -> Result<i32, AccountError> {
        let db_conn = self.get_db_conn()?;
        let user_id = self.get_user_id(&username)?;

        let login_secrets = self.get_login_secrets(user_id, &db_conn, login_secrets_key)?;

        // Validate OTP
        let expected_otp = otp::make_totp(&login_secrets.otp_secret, 30, 0).map_err(|e| {
            log::debug!("Unable to generate current OTP: {}", e);
            AccountError::InternalError
        })?;
        if user_otp != expected_otp {
            return Err(AccountError::InvalidOTP);
        }

        self.login_secrets_cache.insert(username, login_secrets);

        Ok(user_id)
    }

    pub async fn register_user(
        &self,
        new_user: NewUser<'_>,
        query_master_key: &[u8],
        query_registry: &[u8],
        credentials_verifier: &CredentialsVerifier,
    ) -> Result<(), AccountError> {
        use schema::users::dsl::*;

        if !new_user
            .username
            .chars()
            .all(|c: char| char::is_ascii_alphanumeric(&c))
        {
            log::error!("Username with invalid character: '{}'", new_user.username);
            return Err(AccountError::InvalidUsername);
        }

        let db_conn = self.get_db_conn()?;

        // Verify if username is taken
        let result = users
            .filter(username.eq(new_user.username))
            .select(username)
            .execute(&db_conn)
            .map_err(|e| {
                log::error!("Unable to search for username={}: {}", new_user.username, e);
                AccountError::InternalError
            })?;
        if result > 0 {
            log::error!("Username \"{}\" is already taken", new_user.username);
            return Err(AccountError::UnavailableUsername);
        }

        // Register the user
        log::debug!("Registering user: {:?}", new_user);
        let user = diesel::insert_into(schema::users::table)
            .values(&new_user)
            .get_result::<User>(&db_conn)
            .map_err(|e| {
                log::error!("Unable to insert for user={:?}: {}", new_user, e);
                AccountError::InternalError
            })?;
        log::debug!("Successfully registered user: {:?}", user);

        // Get a token for the user
        let token = credentials_verifier.new_token(user.id).await.map_err(|e| {
            log::error!("Unable to get token for new user id={}: {}", user.id, e);
            AccountError::InternalError
        })?;
        let grpc_token = MetadataValue::from_str(&("Bearer: ".to_owned() + token.as_ref()))
            .map_err(|e| {
                log::error!("Unable to prepare token: {}", e);
                AccountError::InternalError
            })?;

        // Ask master_keys service to add private key for user_id
        let mut master_keys_request = tonic::Request::new(MasterKeyUpdateRequest {
            master_key: query_master_key.to_vec(),
        });
        master_keys_request
            .metadata_mut()
            .insert("authorization", grpc_token.clone());
        let master_keys_result = self
            .master_key_client
            .clone()
            .update_master_key(master_keys_request)
            .await
            .map_err(|e| {
                log::error!("Unable to create master key: {}", e);
                AccountError::InternalError
            });
        if let Err(error) = master_keys_result {
            let _ = self.delete_account(user.id).await;
            return Err(error);
        }

        // Ask registry to create entry for user_id
        let mut registry_request = tonic::Request::new(UpdateFileListRequest {
            file_list: query_registry.to_vec(),
            version_tag: 0,
            file_ack: None,
        });
        registry_request
            .metadata_mut()
            .insert("authorization", grpc_token.clone());
        let registry_result = self
            .registry_client
            .clone()
            .update_file_list(registry_request)
            .await
            .map_err(|e| {
                log::error!("Unable to create registry: {}", e);
                AccountError::InternalError
            });
        if let Err(error) = registry_result {
            let _ = self.delete_account(user.id).await;
            return Err(error);
        }

        Ok(())
    }

    pub async fn update_passkey(
        &self,
        user_id: i32,
        new_passkey: String,
        login_secrets_key: &[u8],
    ) -> Result<(), AccountError> {
        use schema::users::dsl::*;

        log::debug!(
            "Updating passkey for user(id={}): {:?}",
            user_id,
            new_passkey
        );

        let db_conn = self.get_db_conn()?;
        db_conn.transaction::<(), AccountError, _>(|| {
            let current_login_secrets =
                self.get_login_secrets(user_id, &db_conn, login_secrets_key)?;

            let new_login_secrets = LoginSecrets {
                passkey: new_passkey,
                otp_secret: current_login_secrets.otp_secret,
            };

            let data = rmp_serde::to_vec(&new_login_secrets).map_err(|e| {
                log::error!("unable to parse serialize login secrets: {}", e);
                AccountError::InternalError
            })?;
            let nonce = secretbox::gen_nonce();
            let key = secretbox::Key::from_slice(login_secrets_key).ok_or_else(|| {
                log::error!("unable to parse acess key");
                AccountError::InternalError
            })?;
            let encrypted_data = secretbox::seal(&data, &nonce, &key);

            diesel::update(users.filter(id.eq(user_id)))
                .set(enc_login_secrets.eq(&encrypted_data))
                .execute(&db_conn)?;

            Ok(())
        })?;

        log::debug!("Successfully deleted user: {:?}", user_id);

        Ok(())
    }

    pub async fn delete_account(&self, user_id: i32) -> Result<()> {
        use schema::users::dsl::*;

        // TODO Delete master_key and registry
        // TODO Verify there are no more password in the registry

        log::debug!("Deleting user: {:?}", user_id);
        diesel::delete(users)
            .filter(id.eq(user_id))
            .execute(&self.db_pool.get()?)?;
        log::debug!("Successfully deleted user: {:?}", user_id);

        Ok(())
    }

    pub fn get_public_key(&self, query_username: &str) -> Result<Vec<u8>, AccountError> {
        use common::schema::users::dsl::*;

        log::debug!("Getting public key for user {}", query_username);

        let db_conn = self.get_db_conn()?;

        let user_enc_public_key = users
            .filter(username.eq(query_username))
            .select(enc_public_key)
            .get_result::<Vec<u8>>(&db_conn)
            .map_err(|e| {
                log::debug!("Unable to load user {}: {}", query_username, e);
                AccountError::DieselError(e)
            })?;

        log::debug!(
            "Public key for user {} is {:?}",
            query_username,
            user_enc_public_key
        );

        Ok(user_enc_public_key)
    }

    fn get_db_conn(&self) -> Result<DbConn, AccountError> {
        self.db_pool.get().map_err(|e| {
            log::debug!("Unable to get DB connection from pool: {}", e);
            AccountError::InternalError
        })
    }

    fn get_login_secrets(
        &self,
        user_id: i32,
        db_conn: &DbConn,
        login_secrets_key: &[u8],
    ) -> Result<LoginSecrets, AccountError> {
        use schema::users::dsl::*;
        log::debug!("Fetching login secrets for user {}", user_id);
        let encrypted_login_secrets = users
            .filter(id.eq(user_id))
            .select(enc_login_secrets)
            .get_result::<Vec<u8>>(db_conn)?;

        log::debug!(
            "Decrypting and deserializing login secrets for user {} with key {:?}",
            user_id,
            login_secrets_key
        );
        LoginSecrets::from_encrypted_slice(&encrypted_login_secrets, login_secrets_key)
    }
}

impl ScramAuthServer<ScramSha256> for AccountManager {
    fn get_password_for_user(&self, query_username: &str) -> Option<ScramPassword> {
        log::debug!("Fetching passkey for user {}", query_username);
        match self.login_secrets_cache.get(query_username) {
            Some(login_secrets) => {
                let passkey = &login_secrets.passkey;
                log::debug!("Passkey for user {} is {}", query_username, passkey);
                let password_salt = randombytes(24);
                Some(ScramPassword::found_secret_password(
                    // TODO Find better way to handle than unwrap
                    ScramSha256::derive(passkey.as_bytes(), &password_salt, 4096).unwrap(),
                    password_salt,
                    4096,
                ))
            }
            // TODO Find better way to handle than unwrap
            None => ScramPassword::not_found::<ScramSha256>().ok(),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct LoginSecrets {
    pub passkey: String,
    pub otp_secret: String,
}
impl LoginSecrets {
    pub fn from_encrypted_slice(
        encrypted_login_secrets: &[u8],
        login_secrets_key: &[u8],
    ) -> Result<LoginSecrets, AccountError> {
        let key = secretbox::Key::from_slice(login_secrets_key).ok_or_else(|| {
            log::error!("unable to parse acess key");
            AccountError::InternalError
        })?;

        let encrypted_data = EncryptedData::from_slice(encrypted_login_secrets)?;
        let nonce = secretbox::Nonce::from_slice(&encrypted_data.nonce).ok_or_else(|| {
            log::error!("unable to parse Nonce");
            AccountError::InternalError
        })?;

        // Decrypt file with access key
        let current_login_secrets = secretbox::open(&encrypted_data.encrypted_data, &nonce, &key)
            .map_err(|_| {
            log::error!("unable to decrypt login secrets");
            AccountError::InternalError
        })?;
        let login_secrets: LoginSecrets = rmp_serde::from_read_ref(&current_login_secrets)
            .map_err(|e| {
                log::error!("unable to parse login secrets: {}", e);
                AccountError::InternalError
            })?;

        Ok(login_secrets)
    }
}
