#[macro_use]
extern crate diesel;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;

use std::sync::{Arc, Mutex};
use std::time::{SystemTime, UNIX_EPOCH};

use anyhow::{Error, Result};
use salak::{Environment, Salak};

use scram_rs::scram::{ScramNonce, ScramParse, ScramServer};
use scram_rs::scram_common::ScramCommon;
use scram_rs::scram_hashing::ScramSha256;
use simple_logger::SimpleLogger;
use tonic::{transport::Server, Response};

use common::api::account::{
    account_server::{Account, AccountServer},
    Credentials, DeleteAccountRequest, DeleteAccountResponse, PublicKeyRequest, PublicKeyResponse,
    RegisterRequest, RegisterResponse, SaltReponse, SaltRequest, ScramClientFinal,
    ScramClientFirst, ScramServerFinal, ScramServerFirst, UpdatePasskeyRequest,
    UpdatePasskeyResponse,
};
use common::config::Config;
use common::credentials::{CredentialsVerifier, UserId};
use common::db;
use common::session::SessionManager;

use account_manager::AccountManager;
mod account_manager;
mod models;

static SESSION_DURATION: u64 = 5 * 60; // In seconds

#[derive(Clone)]
struct LoginSession {
    user_id: UserId,
    scram_server: Arc<Mutex<ScramServer<'static, ScramSha256, AccountManager>>>,
}

pub struct AccountService {
    login_sessions: SessionManager<LoginSession>,
    credentials_verifier: CredentialsVerifier,
    account_manager: &'static AccountManager,
}

impl AccountService {
    pub fn new(
        credentials_verifier: CredentialsVerifier,
        account_manager: &'static AccountManager,
    ) -> AccountService {
        AccountService {
            login_sessions: SessionManager::new(),
            credentials_verifier,
            account_manager,
        }
    }
}

#[tonic::async_trait]
impl Account for AccountService {
    async fn get_salt(
        &self,
        request: tonic::Request<SaltRequest>,
    ) -> Result<tonic::Response<SaltReponse>, tonic::Status> {
        let username = request.into_inner().username;
        let salt = self
            .account_manager
            .get_salt(username.as_ref())
            .map_err(|e| {
                log::error!("Unable to get salt for user {}: {:?}", username, e);
                tonic::Status::internal("internal error")
            })?;
        Ok(tonic::Response::new(SaltReponse { salt }))
    }

    async fn login_init(
        &self,
        request: tonic::Request<ScramClientFirst>,
    ) -> Result<tonic::Response<ScramServerFirst>, tonic::Status> {
        let login_request = request.into_inner();

        let client_first_bytes = base64::decode(&login_request.client_first).map_err(|e| {
            log::error!("Unable to decode client_first: {:?}", e);
            tonic::Status::internal("internal error")
        })?;
        let client_first_string = String::from_utf8(client_first_bytes).map_err(|e| {
            log::error!("Unable to utf8 parse client_first: {:?}", e);
            tonic::Status::internal("internal error")
        })?;
        let client_first_string = client_first_string.strip_prefix("n,,n=").ok_or_else(|| {
            log::error!("Wrong client_first prefix");
            tonic::Status::internal("internal error")
        })?;
        let username_end = client_first_string.find(',').ok_or_else(|| {
            log::error!("Wrong client_first suffix");
            tonic::Status::internal("internal error")
        })?;
        let username = &client_first_string[0..username_end];
        log::debug!("authenticating user: {}", username);
        let user_id = self
            .account_manager
            .pre_auth(
                username.to_string(),
                &login_request.login_secrets_key,
                login_request.otp,
            )
            .await
            .map_err(|e| {
                log::error!("Unable to pre-auth user {}: {:?}", username, e);
                tonic::Status::internal("internal error")
            })?;

        let scramtype = ScramCommon::get_scramtype("SCRAM-SHA-256").map_err(|e| {
            log::error!("Unable to select scram type: {}", e);
            tonic::Status::internal("internal error")
        })?;
        let mut scram_server = ScramServer::<ScramSha256, AccountManager>::new(
            self.account_manager,
            None,
            ScramNonce::none(),
            scramtype,
        )
        .map_err(|e| {
            log::error!("Unable to prepare password for SCRAM: {:?}", e);
            tonic::Status::internal("internal error")
        })?;

        log::debug!("client_first: {:?}", login_request.client_first);
        match scram_server.parse_response_base64(&login_request.client_first) {
            Ok(ScramParse::Output(server_first)) => {
                let session = LoginSession {
                    user_id,
                    scram_server: Arc::new(Mutex::new(scram_server)),
                };

                let session_id = self.login_sessions.new_session(session).map_err(|e| {
                    log::error!("Unable to create session: {:?}", e);
                    tonic::Status::aborted("internal error")
                })?;
                log::debug!("Created session: {:?}", session_id);

                Ok(Response::new(ScramServerFirst {
                    session_id,
                    server_first,
                }))
            }
            Err(e) => {
                log::warn!("Unable to parse client_first: {:?}", e);
                Err(tonic::Status::aborted("invalid client_first message"))
            }
            Ok(ScramParse::Completed) => {
                log::error!("Unexpected ScramParse::Completed after client_first");
                Err(tonic::Status::aborted("invalid client_first message"))
            }
        }
    }

    async fn login_end(
        &self,
        request: tonic::Request<ScramClientFinal>,
    ) -> Result<tonic::Response<ScramServerFinal>, tonic::Status> {
        let session_id = request.get_ref().session_id.clone();
        let client_final = request.get_ref().client_final.clone();

        let session = self
            .login_sessions
            .get(session_id.as_slice())
            .ok_or_else(|| {
                log::debug!("Session not found: {:?}", session_id);
                tonic::Status::invalid_argument("invalid session")
            })?;

        let user_id = session.user_id;

        let server_final = if let Ok(mut session) = session.scram_server.lock() {
            log::debug!("client_final: {:?}", client_final);
            match session.parse_response_base64(&client_final) {
                Ok(ScramParse::Output(server_final)) => Ok(server_final),
                Err(e) => {
                    log::warn!("Unable to handle client_final: {:?}", e);
                    Err(tonic::Status::aborted("Unable to handle client_final"))
                }
                _ => Err(tonic::Status::aborted(
                    "Unable to handle client_final: unknown error",
                )),
            }
        } else {
            log::warn!("Unable to lock session mutex");
            Err(tonic::Status::aborted("Unable to handle client_final"))
        }?;

        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .map_err(|e| {
                log::error!("Unable to get current timestamp: {:?}", e);
                tonic::Status::internal("internal error")
            })?
            .as_secs();

        let token = self
            .credentials_verifier
            .new_token(user_id)
            .await
            .map_err(|_| tonic::Status::resource_exhausted("invalid token"))?;

        let credentials = Some(Credentials {
            token,
            expiration: now + SESSION_DURATION,
        });

        Ok(Response::new(ScramServerFinal {
            server_final,
            credentials,
        }))
    }

    async fn register(
        &self,
        request: tonic::Request<RegisterRequest>,
    ) -> Result<tonic::Response<RegisterResponse>, tonic::Status> {
        let register_request = request.into_inner();

        let new_user = models::NewUser {
            username: register_request.username.as_ref(),
            salt: register_request.salt.as_ref(),
            enc_login_secrets: register_request.encrypted_login_secrets.as_ref(),
            enc_public_key: register_request.enc_public_key.as_ref(),
        };
        self.account_manager
            .register_user(
                new_user,
                register_request.encrypted_private_key.as_ref(),
                register_request.encrypted_registry.as_ref(),
                &self.credentials_verifier,
            )
            .await
            .map_err(|e| {
                let e_str: &str = e.into();
                log::error!("unable to register user: {}", e_str);
                tonic::Status::internal(e_str)
            })?;

        Ok(Response::new(RegisterResponse {}))
    }

    async fn update_passkey(
        &self,
        request: tonic::Request<UpdatePasskeyRequest>,
    ) -> Result<tonic::Response<UpdatePasskeyResponse>, tonic::Status> {
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|_| tonic::Status::resource_exhausted("invalid token"))?;

        let passkey_request = request.into_inner();

        self.account_manager
            .update_passkey(
                user_id,
                passkey_request.passkey,
                &passkey_request.login_secrets_key,
            )
            .await
            .map_err(|e| {
                log::error!("unable to update passkey for user: {:?}", e);
                tonic::Status::internal("unable to update passkey")
            })?;

        Ok(Response::new(UpdatePasskeyResponse {}))
    }

    async fn delete_account(
        &self,
        request: tonic::Request<DeleteAccountRequest>,
    ) -> Result<tonic::Response<DeleteAccountResponse>, tonic::Status> {
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|_| tonic::Status::resource_exhausted("invalid token"))?;

        self.account_manager
            .delete_account(user_id)
            .await
            .map_err(|e| {
                log::error!("unable to delete account for user: {}", e);
                tonic::Status::internal("unable to delete account")
            })?;

        Ok(Response::new(DeleteAccountResponse {}))
    }

    async fn get_public_key(
        &self,
        request: tonic::Request<PublicKeyRequest>,
    ) -> Result<tonic::Response<PublicKeyResponse>, tonic::Status> {
        let username = request.into_inner().username;
        let enc_public_key = self
            .account_manager
            .get_public_key(username.as_ref())
            .map_err(|e| {
                log::error!("Unable to get public key for user {}: {:?}", username, e);
                tonic::Status::internal("internal error")
            })?;
        Ok(tonic::Response::new(PublicKeyResponse { enc_public_key }))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    SimpleLogger::from_env().init()?;

    log::info!("Zorpass account server");

    sodiumoxide::init().map_err(|_| Error::msg("unable to init sodiumoxide"))?;

    let env = Salak::builder().build().unwrap();
    let config = env.get::<Config>().unwrap();

    let db_url = config.database.as_db_url();
    let db_pool = db::connection_pool(db_url, 2)?;
    {
        let db_conn = db_pool.get()?;
        db::run_migrations(&db_conn)?;
    }

    let account_manager: &'static AccountManager =
        Box::leak(Box::new(AccountManager::new(db_pool, &config).await?));

    let auth_addr = config.authentication.as_http_url();
    let credentials_verifier = CredentialsVerifier::new(auth_addr).await?;
    let service = AccountService::new(credentials_verifier, &account_manager);
    let addr = format!("0.0.0.0:{}", config.account.port).parse()?;
    log::info!("Server listening on {}", addr);

    Server::builder()
        .add_service(AccountServer::new(service))
        .serve(addr)
        .await?;

    Ok(())
}
