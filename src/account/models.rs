use common::schema::users;

#[derive(Insertable, Debug)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub salt: &'a [u8],
    pub enc_login_secrets: &'a [u8],
    pub enc_public_key: &'a [u8],
}

#[derive(Queryable, Debug)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub salt: Vec<u8>,
    pub enc_login_secrets: Vec<u8>,
    pub enc_public_key: Vec<u8>,
}
