use std::time::{SystemTime, UNIX_EPOCH};

use salak::{Environment, Salak};
use simple_logger::SimpleLogger;
use tonic::{transport::Server, Response};

use common::api::{
    credentials::credentials_server::{Credentials, CredentialsServer},
    credentials::{
        NewTokenRequest, NewTokenResponse, VerifyCredentialsRequest, VerifyCredentialsResponse,
    },
};
use common::config::Config;
use common::credentials::UserId;
use common::session::SessionManager;

static SESSION_DURATION: u64 = 2 * 60; // In seconds

#[derive(Clone, Debug)]
struct AuthSession {
    user_id: UserId,
    expiration: u64,
}

pub struct CredentialsService {
    session_manager: SessionManager<AuthSession>,
}
impl CredentialsService {
    pub fn new() -> CredentialsService {
        CredentialsService {
            session_manager: SessionManager::new(),
        }
    }
}
impl Default for CredentialsService {
    fn default() -> Self {
        Self::new()
    }
}

#[tonic::async_trait]
impl Credentials for CredentialsService {
    async fn new_token(
        &self,
        request: tonic::Request<NewTokenRequest>,
    ) -> Result<tonic::Response<NewTokenResponse>, tonic::Status> {
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .map_err(|e| {
                log::error!("Unable to get current timestamp: {:?}", e);
                tonic::Status::internal("internal error")
            })?
            .as_secs();

        let user_id = request.into_inner().user_id;
        let session = AuthSession {
            user_id,
            expiration: now + SESSION_DURATION,
        };
        let token = self.session_manager.new_session(session).map_err(|e| {
            log::error!("Unable to create auth session: {:?}", e);
            tonic::Status::internal("internal error")
        })?;

        log::debug!(
            "Generated new session for user_id={} with token={:?}",
            user_id,
            token
        );
        Ok(Response::new(NewTokenResponse {
            token: base64::encode(token.as_slice()),
        }))
    }

    async fn verify_credentials(
        &self,
        request: tonic::Request<VerifyCredentialsRequest>,
    ) -> Result<tonic::Response<VerifyCredentialsResponse>, tonic::Status> {
        let token = base64::decode(request.into_inner().token).map_err(|e| {
            log::error!("Unable to create auth session: {:?}", e);
            tonic::Status::internal("internal error")
        })?;
        log::info!("Verifying token {:?}", token);

        match self.session_manager.get(token.as_slice()) {
            Some(session) => {
                let now = SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .map_err(|e| {
                        log::error!("Unable to get current timestamp: {:?}", e);
                        tonic::Status::internal("internal error")
                    })?
                    .as_secs();
                if session.expiration > now {
                    Ok(Response::new(VerifyCredentialsResponse {
                        user_id: session.user_id,
                    }))
                } else {
                    log::debug!("Token expired: now={} token={:?}", now, session);
                    Err(tonic::Status::resource_exhausted("invalid token"))
                }
            }
            None => {
                log::debug!("Token not found: {:?}", token);
                Err(tonic::Status::resource_exhausted("invalid token"))
            }
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    SimpleLogger::from_env().init()?;

    log::info!("Zorpass authentication server");

    let env = Salak::builder().build().unwrap();
    let config = env.get::<Config>().unwrap();

    let addr = format!("0.0.0.0:{}", config.authentication.port).parse()?;
    log::info!("Server listening on {}", addr);

    let service = CredentialsService::new();
    Server::builder()
        .add_service(CredentialsServer::new(service))
        .serve(addr)
        .await?;

    Ok(())
}
