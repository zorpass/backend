use salak::*;

#[derive(FromEnvironment, Debug)]
pub struct Endpoint {
    pub host: String,
    pub port: u16,
    pub tls: bool,
}
impl Endpoint {
    pub fn as_http_url(&self) -> String {
        let scheme = if self.tls { "https" } else { "http" };
        format!("{}://{}:{}", scheme, self.host, self.port)
    }
}

#[derive(FromEnvironment, Debug)]
pub struct DatabaseConfig {
    pub host: String,
    pub port: u16,
    pub user: String,
    pub password: String,
    pub db: String,
}
impl DatabaseConfig {
    pub fn as_db_url(&self) -> String {
        format!(
            "postgres://{}:{}@{}:{}/{}",
            self.user, self.password, self.host, self.port, self.db
        )
    }
}

#[derive(FromEnvironment, Debug)]
#[salak(prefix = "zorpass")]
pub struct Config {
    pub authentication: Endpoint,
    pub account: Endpoint,
    pub masterkeys: Endpoint,
    pub registry: Endpoint,
    pub database: DatabaseConfig,
}
