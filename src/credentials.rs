use anyhow::{Error, Result};
use tonic::Request;

use crate::api::credentials::{
    credentials_client::CredentialsClient, NewTokenRequest, VerifyCredentialsRequest,
};

pub type UserId = i32;

pub struct CredentialsVerifier {
    client: CredentialsClient<tonic::transport::Channel>,
}

impl CredentialsVerifier {
    pub async fn new(endpoint: String) -> Result<CredentialsVerifier> {
        log::debug!("Connecting to {}", &endpoint);
        let channel = tonic::transport::Channel::from_shared(endpoint)?
            .connect()
            .await?;
        let client = CredentialsClient::new(channel);

        log::debug!("Connected");
        Ok(CredentialsVerifier { client })
    }

    pub async fn new_token(&self, user_id: UserId) -> Result<String> {
        let request = tonic::Request::new(NewTokenRequest { user_id });

        log::debug!("Verifying credentials for token {:?}", request);
        let response = self.client.clone().new_token(request).await?.into_inner();
        log::debug!("Verification RESPONSE={:?}", response);

        Ok(response.token)
    }

    pub async fn verify_credentials<T>(&self, request: &Request<T>) -> Result<UserId> {
        let authorization = request
            .metadata()
            .get("authorization")
            .ok_or_else(|| {
                log::debug!("authorization field not found in request");
                Error::msg("authorization field not found in request")
            })?
            .to_str()
            .map_err(|e| {
                log::debug!("unable to parse authorization field as a string: {}", e);
                Error::msg("unable to parse authorization field as a string")
            })?;

        let token = authorization
            .strip_prefix("Bearer: ")
            .ok_or_else(|| {
                log::debug!("token is not of type 'Bearer'");
                Error::msg("wrong token type")
            })?
            .to_string();
        self.verify_token(token).await
    }

    pub async fn verify_token(&self, token: String) -> Result<UserId> {
        let request = tonic::Request::new(VerifyCredentialsRequest { token });

        log::debug!("Verifying credentials for token {:?}", request);
        let response = self
            .client
            .clone()
            .verify_credentials(request)
            .await?
            .into_inner();
        log::debug!("Verification RESPONSE={:?}", response);

        Ok(response.user_id)
    }
}
