pub enum CryptoError {
    ParsingError,
    SerializingError,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct EncryptedData {
    pub nonce: Vec<u8>,
    pub encrypted_data: Vec<u8>,
}
impl EncryptedData {
    pub fn from_slice(data: &[u8]) -> Result<EncryptedData, CryptoError> {
        rmp_serde::from_read_ref(data).map_err(|e| {
            log::error!("unable to parse encrypted data: {}", e);
            CryptoError::ParsingError
        })
    }

    pub fn to_vec(&self) -> Result<Vec<u8>, CryptoError> {
        rmp_serde::to_vec(self).map_err(|e| {
            log::error!("unable to parse serialize encrypted data: {}", e);
            CryptoError::SerializingError
        })
    }
}
