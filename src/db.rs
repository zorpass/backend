use diesel::r2d2::{ConnectionManager, Pool, PoolError, PooledConnection};
use diesel::PgConnection;
use diesel_derive_enum::*;
use diesel_migrations::{embed_migrations, RunMigrationsError};

embed_migrations!();

pub type DbPool = Pool<ConnectionManager<PgConnection>>;
pub type DbConn = PooledConnection<ConnectionManager<PgConnection>>;

pub fn connection_pool(
    connection_string: impl Into<String>,
    max_size: u32,
) -> Result<DbPool, PoolError> {
    let manager = ConnectionManager::<PgConnection>::new(connection_string.into());
    Pool::builder().max_size(max_size).build(manager)
}

pub fn run_migrations(db_conn: &DbConn) -> Result<(), RunMigrationsError> {
    embedded_migrations::run(db_conn)
}

#[derive(DbEnum, Debug, PartialEq)]
#[PgType = "file_state"]
#[DieselType = "File_state"]
pub enum FileState {
    Fresh,
    Inactive,
    Active,
}
