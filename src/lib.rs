#[macro_use]
extern crate diesel;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;
#[macro_use]
extern crate diesel_migrations;

pub mod config;
pub mod credentials;
pub mod crypto;
pub mod db;
pub mod session;

pub mod schema;

pub mod api {
    pub mod account {
        tonic::include_proto!("account");
    }

    pub mod credentials {
        tonic::include_proto!("credentials");
    }

    pub mod master_keys {
        tonic::include_proto!("master_keys");
    }

    pub mod password_registry {
        tonic::include_proto!("password_registry");
    }
}
