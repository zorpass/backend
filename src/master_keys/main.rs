#[macro_use]
extern crate diesel;

use anyhow::Result;
use diesel::{prelude::*, RunQueryDsl};
use salak::{Environment, Salak};
use simple_logger::SimpleLogger;
use tonic::{transport::Server, Response};

use common::api::{
    master_keys::master_key_server::{MasterKey, MasterKeyServer},
    master_keys::{
        MasterKeyRequest, MasterKeyResponse, MasterKeyUpdateRequest, MasterKeyUpdateResponse,
    },
};
use common::config::Config;
use common::credentials::CredentialsVerifier;
use common::db;
use models::{Masterkey, NewMasterkey};
mod models;

pub struct MasterKeyService {
    credentials_verifier: CredentialsVerifier,
    db_pool: db::DbPool,
}
impl MasterKeyService {
    pub fn new(credentials_verifier: CredentialsVerifier, db_pool: db::DbPool) -> MasterKeyService {
        MasterKeyService {
            credentials_verifier,
            db_pool,
        }
    }
}

#[tonic::async_trait]
impl MasterKey for MasterKeyService {
    async fn get_master_key(
        &self,
        request: tonic::Request<MasterKeyRequest>,
    ) -> Result<tonic::Response<MasterKeyResponse>, tonic::Status> {
        use common::schema::masterkeys::dsl::*;

        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        log::debug!("Serving master key for user {}", user_id);

        let db_conn = self.db_pool.get().map_err(|e| {
            log::debug!("Unable to get DB connection from pool: {}", e);
            tonic::Status::resource_exhausted("unable to get DB connection from pool")
        })?;
        let master_key_result = masterkeys
            .filter(id.eq(user_id))
            .load::<Masterkey>(&db_conn)
            .map_err(|e| {
                log::debug!("Unable to load master key: {}", e);
                tonic::Status::internal("Unable to load master key")
            })?;

        if master_key_result.is_empty() {
            log::debug!("No master key found");
            return Err(tonic::Status::internal("no master key found"));
        }

        Ok(Response::new(MasterKeyResponse {
            master_key: master_key_result[0].master_key.clone(),
        }))
    }

    async fn update_master_key(
        &self,
        request: tonic::Request<MasterKeyUpdateRequest>,
    ) -> Result<tonic::Response<MasterKeyUpdateResponse>, tonic::Status> {
        use common::schema::masterkeys;

        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        let request_data = request.into_inner();
        let new_master_key = NewMasterkey {
            id: user_id,
            master_key: request_data.master_key.as_slice(),
        };
        let db_conn = self.db_pool.get().map_err(|e| {
            log::debug!("Unable to get DB connection from pool: {}", e);
            tonic::Status::resource_exhausted("unable to get DB connection from pool")
        })?;
        diesel::insert_into(masterkeys::table)
            .values(&new_master_key)
            .on_conflict(masterkeys::id)
            .do_update()
            .set(&new_master_key)
            .execute(&db_conn)
            .map_err(|e| {
                log::debug!("Unable to upsert master key: {}", e);
                tonic::Status::internal("Unable to upsert master key")
            })?;

        log::debug!(
            "Successfully registered master key for user {}: {:?}",
            user_id,
            new_master_key
        );

        Ok(tonic::Response::new(MasterKeyUpdateResponse {}))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    SimpleLogger::from_env().init()?;

    log::info!("Zorpass master keys server");
    let env = Salak::builder().build().unwrap();
    let config = env.get::<Config>().unwrap();

    let auth_addr = config.authentication.as_http_url();
    let credentials_verifier = CredentialsVerifier::new(auth_addr).await?;

    let db_url = config.database.as_db_url();
    let db_pool = db::connection_pool(db_url, 2)?;

    let service = MasterKeyService::new(credentials_verifier, db_pool);

    let addr = format!("0.0.0.0:{}", config.masterkeys.port).parse()?;
    log::info!("Server listening on {}", addr);

    Server::builder()
        .add_service(MasterKeyServer::new(service))
        .serve(addr)
        .await?;

    Ok(())
}
