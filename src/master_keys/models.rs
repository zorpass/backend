use common::schema::masterkeys;

#[derive(AsChangeset, Insertable, Debug)]
#[table_name = "masterkeys"]
pub struct NewMasterkey<'a> {
    pub id: i32,
    pub master_key: &'a [u8],
}

#[derive(Queryable, Debug)]
pub struct Masterkey {
    pub id: i32,
    pub master_key: Vec<u8>,
}
