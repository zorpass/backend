#[macro_use]
extern crate diesel;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;

use anyhow::Result;
use salak::{Environment, Salak};
use simple_logger::SimpleLogger;
use tonic::{transport::Server, Request, Response};

use common::api::{
    password_registry::password_registry_server::{PasswordRegistry, PasswordRegistryServer},
    password_registry::*,
};
use common::config::Config;
use common::credentials::CredentialsVerifier;
use common::db::connection_pool;
use registry_manager::{RegistryError, RegistryManager};
mod models;
mod registry_manager;

pub struct PasswordRegistryService {
    credentials_verifier: CredentialsVerifier,
    registry_manager: RegistryManager,
}
impl PasswordRegistryService {
    pub fn new(
        credentials_verifier: CredentialsVerifier,
        registry_manager: RegistryManager,
    ) -> PasswordRegistryService {
        PasswordRegistryService {
            credentials_verifier,
            registry_manager,
        }
    }
}

#[tonic::async_trait]
impl PasswordRegistry for PasswordRegistryService {
    async fn get_file_list(
        &self,
        request: Request<FileListRequest>,
    ) -> Result<Response<FileListResponse>, tonic::Status> {
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|error| {
                log::info!("Invalid token: {}", error);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        let (file_list, version_tag) =
            self.registry_manager
                .get_registry(user_id)
                .map_err(|e| match e {
                    RegistryError::InternalError => tonic::Status::internal("internal error"),
                    _ => tonic::Status::internal("internal error"),
                })?;

        Ok(Response::new(FileListResponse {
            file_list,
            version_tag,
        }))
    }

    async fn update_file_list(
        &self,
        request: Request<UpdateFileListRequest>,
    ) -> Result<Response<UpdateFileListResponse>, tonic::Status> {
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        let request_data = request.into_inner();
        self.registry_manager
            .update_registry(&request_data.file_list, user_id, request_data.version_tag)
            .map_err(|e| match e {
                RegistryError::InternalError => tonic::Status::internal("internal error"),
                _ => tonic::Status::internal("internal error"),
            })?;

        if let Some(file_access) = request_data.file_ack {
            log::debug!("Found a file to ack: id={}", file_access.file_id);
            self.registry_manager
                .activate_file(user_id, file_access.file_id, &file_access.access_key)
                .map_err(|e| match e {
                    RegistryError::InternalError => tonic::Status::internal("internal error"),
                    _ => tonic::Status::internal("internal error"),
                })?;
        }

        Ok(Response::new(UpdateFileListResponse {}))
    }

    async fn get_file(
        &self,
        request: Request<FileRequest>,
    ) -> Result<Response<FileResponse>, tonic::Status> {
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        let file_access = request.into_inner();
        let enc_file_data = self
            .registry_manager
            .get_file(file_access.file_id, user_id, &file_access.access_key)
            .map_err(|e| match e {
                RegistryError::InternalError => tonic::Status::internal("internal error"),
                _ => tonic::Status::internal("internal error"),
            })?;

        Ok(Response::new(FileResponse { enc_file_data }))
    }

    async fn add_file(
        &self,
        request: Request<AddFileRequest>,
    ) -> Result<Response<AddFileResponse>, tonic::Status> {
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        let request_data = request.into_inner();
        let (file_id, access_key) = self
            .registry_manager
            .add_file(user_id, request_data.enc_file_data)
            .map_err(|e| match e {
                RegistryError::InternalError => tonic::Status::internal("internal error"),
                _ => tonic::Status::internal("internal error"),
            })?;

        Ok(Response::new(AddFileResponse {
            file_id,
            access_key,
        }))
    }

    async fn update_file(
        &self,
        request: Request<UpdateFileRequest>,
    ) -> Result<Response<UpdateFileResponse>, tonic::Status> {
        log::info!("Received file update request");
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        let request_data = request.into_inner();
        self.registry_manager
            .update_file(
                user_id,
                request_data.file_id,
                &request_data.access_key,
                request_data.file_data,
            )
            .map_err(|e| match e {
                RegistryError::InternalError => tonic::Status::internal("internal error"),
                _ => tonic::Status::internal("internal error"),
            })?;

        Ok(Response::new(UpdateFileResponse {}))
    }

    async fn remove_file(
        &self,
        request: Request<RemoveFileRequest>,
    ) -> Result<Response<RemoveFileResponse>, tonic::Status> {
        log::info!("Received file removal request");
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        let request_data = request.into_inner();
        self.registry_manager
            .remove_file(user_id, request_data.file_id, &request_data.access_key)
            .map_err(|e| match e {
                RegistryError::InternalError => tonic::Status::internal("internal error"),
                _ => tonic::Status::internal("internal error"),
            })?;

        Ok(Response::new(RemoveFileResponse {}))
    }

    async fn share_file(
        &self,
        request: Request<ShareFileRequest>,
    ) -> Result<Response<ShareFileResponse>, tonic::Status> {
        log::info!("Received file sharing request");
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;
        let request_data = request.into_inner();
        self.registry_manager
            .share_file(user_id, request_data.recipient, request_data.file_data)
            .map_err(|e| match e {
                RegistryError::InternalError => tonic::Status::internal("internal error"),
                _ => tonic::Status::internal("internal error"),
            })?;

        Ok(Response::new(ShareFileResponse {}))
    }

    async fn list_share_offers(
        &self,
        request: Request<ListeShareOffersRequest>,
    ) -> Result<Response<ListeShareOffersResponse>, tonic::Status> {
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        let offers = self
            .registry_manager
            .list_share_offers(user_id)
            .map_err(|e| match e {
                RegistryError::InternalError => tonic::Status::internal("internal error"),
                _ => tonic::Status::internal("internal error"),
            })?;

        let share_offers = offers
            .iter()
            .map(|offer| liste_share_offers_response::ShareOffer {
                id: offer.0,
                offer: offer.1.clone(),
            })
            .collect();

        Ok(Response::new(ListeShareOffersResponse { share_offers }))
    }

    async fn remove_offer(
        &self,
        request: Request<RemoveOfferRequest>,
    ) -> Result<Response<RemoveOfferResponse>, tonic::Status> {
        log::info!("Received share offer remove request");
        let user_id = self
            .credentials_verifier
            .verify_credentials(&request)
            .await
            .map_err(|e| {
                log::debug!("Unverified token: {}", e);
                tonic::Status::resource_exhausted("invalid token")
            })?;

        self.registry_manager
            .remove_share_offer(user_id, request.into_inner().id)
            .map_err(|e| match e {
                RegistryError::InternalError => tonic::Status::internal("internal error"),
                _ => tonic::Status::internal("internal error"),
            })?;

        Ok(Response::new(RemoveOfferResponse {}))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    SimpleLogger::from_env().init()?;

    log::info!("Zorpass registry server");

    let env = Salak::builder().build().unwrap();
    let config = env.get::<Config>().unwrap();

    let auth_addr = config.authentication.as_http_url();
    let credentials_verifier = CredentialsVerifier::new(auth_addr).await?;

    let db_url = config.database.as_db_url();
    let db_pool = connection_pool(db_url, 2)?;

    let registry_manager = RegistryManager::new(db_pool);
    let service = PasswordRegistryService::new(credentials_verifier, registry_manager);

    let addr = format!("0.0.0.0:{}", config.registry.port).parse()?;
    log::info!("Server listening on {}", addr);

    Server::builder()
        .add_service(PasswordRegistryServer::new(service))
        .serve(addr)
        .await?;

    Ok(())
}
