use common::db::FileState;
use common::schema::file_lists;
use common::schema::files;
use common::schema::share_offers;

#[derive(AsChangeset, Insertable, Debug)]
#[table_name = "file_lists"]
pub struct NewFileList<'a> {
    pub user_id: i32,
    pub file_list: &'a [u8],
    pub version_tag: i64,
}

#[derive(Queryable, Debug)]
pub struct FileList {
    pub user_id: i32,
    pub file_list: Vec<u8>,
    pub version_tag: i64,
}

#[derive(AsChangeset, Insertable, Debug)]
#[table_name = "files"]
pub struct NewFile<'a> {
    pub file_data: &'a [u8],
    pub state: FileState,
}

#[derive(Queryable, Debug)]
pub struct File {
    pub id: i32,
    pub file_data: Vec<u8>,
    pub state: FileState,
}

#[derive(Queryable, Debug)]
pub struct UserInfo {
    pub id: i32,
    pub enc_public_key: Vec<u8>,
}

#[derive(AsChangeset, Insertable, Debug)]
#[table_name = "share_offers"]
pub struct NewShareOffer<'a> {
    pub user_id: i32,
    pub share_offer: &'a [u8],
}

#[derive(Queryable, Debug)]
pub struct ShareOffer {
    pub id: i32,
    pub user_id: i32,
    pub share_offer: Vec<u8>,
}
