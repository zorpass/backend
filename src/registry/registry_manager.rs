use diesel::{self, prelude::*, RunQueryDsl};
use sodiumoxide::{
    crypto::{box_, sealedbox, secretbox},
    randombytes,
};

use crate::models::{File, FileList, NewFile, NewFileList, NewShareOffer, ShareOffer, UserInfo};
use common::crypto::{CryptoError, EncryptedData};
use common::db::{DbPool, FileState};

pub enum RegistryError {
    InvalidUserId,
    InvalidFileId,
    AlreadyActivated,
    Unauthorized,
    DieselError(diesel::result::Error),
    InternalError,
    VersionTagMismatch,
}
impl From<diesel::result::Error> for RegistryError {
    fn from(e: diesel::result::Error) -> Self {
        RegistryError::DieselError(e)
    }
}
impl From<CryptoError> for RegistryError {
    fn from(_: CryptoError) -> Self {
        RegistryError::InternalError
    }
}

pub struct RegistryManager {
    db_pool: DbPool,
}
impl RegistryManager {
    pub fn new(db_pool: DbPool) -> RegistryManager {
        RegistryManager { db_pool }
    }

    pub fn get_registry(&self, req_user_id: i32) -> Result<(Vec<u8>, i64), RegistryError> {
        use common::schema::file_lists::dsl::*;

        let db_conn = self.get_db_conn()?;
        let result = file_lists
            .filter(user_id.eq(req_user_id))
            .get_result::<FileList>(&db_conn)
            .map_err(|e| {
                log::error!(
                    "Unable to search for file_lists of user_id={}: {}",
                    req_user_id,
                    e
                );
                RegistryError::DieselError(e)
            })?;

        Ok((result.file_list, result.version_tag))
    }

    pub fn update_registry(
        &self,
        file_list: &[u8],
        user_id: i32,
        version_tag: i64,
    ) -> Result<(), RegistryError> {
        use common::schema::file_lists::dsl;

        let db_conn = self.get_db_conn()?;

        db_conn.transaction(|| {
            let version_tag_result = dsl::file_lists
                .filter(dsl::user_id.eq(user_id))
                .select(dsl::version_tag)
                .get_result::<i64>(&db_conn)
                .optional()
                .map_err(|e| {
                    log::error!(
                        "Unable to search for file_lists of user_id={}: {}",
                        user_id,
                        e
                    );
                    RegistryError::DieselError(e)
                })?;

            if let Some(db_version_tag) = version_tag_result {
                log::debug!("version tag found: {}", db_version_tag);
                if db_version_tag != version_tag {
                    log::debug!(
                        "version tags do not match: request={} != db={}",
                        version_tag,
                        db_version_tag
                    );
                    return Err(RegistryError::VersionTagMismatch);
                }
            } else {
                log::debug!("No registry found. Creating a new one.");
            }

            let mut version_tag_bytes = [0; 8];
            randombytes::randombytes_into(&mut version_tag_bytes);
            let version_tag = i64::from_be_bytes(version_tag_bytes);

            let new_file_list = NewFileList {
                user_id,
                file_list,
                version_tag,
            };
            diesel::insert_into(dsl::file_lists)
                .values(&new_file_list)
                .on_conflict(dsl::user_id)
                .do_update()
                .set(&new_file_list)
                .execute(&db_conn)
                .map_err(|e| {
                    log::debug!("Unable to upsert registry: {}", e);
                    RegistryError::DieselError(e)
                })?;

            log::debug!(
                "Successfully registered registry for user {}: {:?}",
                user_id,
                new_file_list
            );

            Ok(())
        })
    }

    pub fn add_file(&self, owner_id: i32, data: Vec<u8>) -> Result<(i32, Vec<u8>), RegistryError> {
        use common::schema::files::dsl::*;

        let db_conn = self.get_db_conn()?;

        let file_info = FileInfo { owner_id, data };
        let access_key = secretbox::gen_key();
        let encrypted_file_info = file_info.to_encrypted(access_key.as_ref())?;
        let file = encrypted_file_info.to_vec()?;

        let new_file = NewFile {
            file_data: &file,
            state: FileState::Fresh,
        };
        let file_id = diesel::insert_into(files)
            .values(&new_file)
            .get_result::<File>(&db_conn)
            .map_err(|e| {
                log::debug!("Unable to insert file in DB: {}", e);
                RegistryError::DieselError(e)
            })?
            .id;

        log::debug!(
            "Successfully added file for user {}: file_id={:?}",
            owner_id,
            file_id
        );

        Ok((file_id, access_key.as_ref().to_vec()))
    }

    pub fn get_file(
        &self,
        file_id: i32,
        owner_id: i32,
        access_key: &[u8],
    ) -> Result<Vec<u8>, RegistryError> {
        use common::schema::files::dsl::*;

        let db_conn = self.db_pool.get().map_err(|e| {
            log::debug!("Unable to get DB connection from pool: {}", e);
            RegistryError::InternalError
        })?;

        let file = files
            .filter(id.eq(file_id))
            .get_result::<File>(&db_conn)
            .map_err(|e| {
                log::error!(
                    "Unable to search for file_lists of file_id={}: {}",
                    file_id,
                    e
                );
                RegistryError::InternalError
            })?;

        let file_info = FileInfo::from_encrypted_file(&file.file_data, access_key)?;
        if file_info.owner_id != owner_id {
            return Err(RegistryError::Unauthorized);
        }

        Ok(file_info.data)
    }

    pub fn activate_file(
        &self,
        user_id: i32,
        file_id: i32,
        access_key: &[u8],
    ) -> Result<(), RegistryError> {
        use common::schema::files::dsl::*;

        let db_conn = self.get_db_conn()?;

        // Fetch file content
        let file = files
            .filter(id.eq(file_id))
            .get_result::<File>(&db_conn)
            .map_err(|e| {
                log::error!("unable to fetch file info: {}", e);
                RegistryError::DieselError(e)
            })?;
        if file.state == FileState::Active {
            return Err(RegistryError::AlreadyActivated);
        }

        // Verify file belongs to user
        let file_info = FileInfo::from_encrypted_file(&file.file_data, access_key)?;
        if file_info.owner_id != user_id {
            return Err(RegistryError::Unauthorized);
        }

        // Update file state
        diesel::update(files.filter(id.eq(file_id)))
            .set(state.eq(FileState::Active))
            .execute(&db_conn)?;

        Ok(())
    }

    pub fn update_file(
        &self,
        user_id: i32,
        file_id: i32,
        access_key: &[u8],
        req_file_data: Vec<u8>,
    ) -> Result<(), RegistryError> {
        use common::schema::files::dsl::*;
        log::debug!("Updating file {} for user {}", file_id, user_id);

        let db_conn = self.get_db_conn()?;

        // Fetch file content
        log::debug!("Fetching file details for id={}", file_id);
        let file = files
            .filter(id.eq(file_id))
            .get_result::<File>(&db_conn)
            .map_err(|e| {
                log::error!("unable to fetch file info: {}", e);
                RegistryError::DieselError(e)
            })?;

        // Verify file belongs to user
        let mut file_info = FileInfo::from_encrypted_file(&file.file_data, access_key)?;
        if file_info.owner_id != user_id {
            return Err(RegistryError::Unauthorized);
        }

        // Update file content
        file_info.data = req_file_data;
        let new_file_data = file_info.to_encrypted(access_key)?.to_vec()?;

        // Update file state
        diesel::update(files.filter(id.eq(file_id)))
            .set(file_data.eq(new_file_data))
            .execute(&db_conn)?;

        Ok(())
    }

    pub fn remove_file(
        &self,
        user_id: i32,
        file_id: i32,
        access_key: &[u8],
    ) -> Result<(), RegistryError> {
        use common::schema::files::dsl::*;
        log::debug!("Updating file {} for user {}", file_id, user_id);

        let db_conn = self.get_db_conn()?;

        // Fetch file content
        log::debug!("Fetching file details for id={}", file_id);
        let file = files
            .filter(id.eq(file_id))
            .get_result::<File>(&db_conn)
            .map_err(|e| {
                log::error!("unable to fetch file info: {}", e);
                RegistryError::DieselError(e)
            })?;

        // Verify file belongs to user
        let file_info = FileInfo::from_encrypted_file(&file.file_data, access_key)?;
        if file_info.owner_id != user_id {
            return Err(RegistryError::Unauthorized);
        }

        // Remove file
        diesel::delete(files)
            .filter(id.eq(file_id))
            .execute(&db_conn)?;

        Ok(())
    }

    pub fn share_file(
        &self,
        sender_id: i32,
        recipient: String,
        data: Vec<u8>,
    ) -> Result<(), RegistryError> {
        log::debug!(
            "Sharing a file from user {} to user {}",
            sender_id,
            recipient
        );
        let db_conn = self.get_db_conn()?;

        // Get recipient's public key
        let recipient_info = self.get_user_info(&recipient)?;
        let sender = {
            use common::schema::users::dsl::*;
            users
                .filter(id.eq(sender_id))
                .select(username)
                .get_result::<String>(&db_conn)?
        };

        // Encrypt (sender_id, file) with PubKey
        let outer_share_offer = OuterShareOffer {
            sender,
            details: data,
        };
        let serialized_offer = rmp_serde::to_vec(&outer_share_offer).map_err(|e| {
            log::error!("unable to parse serialize share offer: {}", e);
            RegistryError::InternalError
        })?;
        let key = box_::PublicKey::from_slice(&recipient_info.enc_public_key).ok_or_else(|| {
            log::error!("unable to parse public key");
            RegistryError::InternalError
        })?;
        let share_offer = sealedbox::seal(&serialized_offer, &key);

        // Save encrypted file in share_offers table
        {
            use common::schema::share_offers::dsl;
            let new_share_offer = NewShareOffer {
                user_id: recipient_info.id,
                share_offer: &share_offer,
            };
            diesel::insert_into(dsl::share_offers)
                .values(&new_share_offer)
                .execute(&db_conn)
        }?;

        Ok(())
    }

    pub fn list_share_offers(
        &self,
        recipient_id: i32,
    ) -> Result<Vec<(i32, Vec<u8>)>, RegistryError> {
        use common::schema::share_offers::dsl::*;
        log::debug!("Fetching list of share offers for user {}", recipient_id);
        let db_conn = self.get_db_conn()?;

        let offers = share_offers
            .filter(user_id.eq(recipient_id))
            .load::<ShareOffer>(&db_conn)?;
        log::debug!(
            "Returning {} share offers for user {}",
            offers.len(),
            recipient_id
        );
        Ok(offers
            .iter()
            .map(|offer| (offer.id, offer.share_offer.clone()))
            .collect())
    }

    pub fn remove_share_offer(
        &self,
        recipient_id: i32,
        offer_id: i32,
    ) -> Result<(), RegistryError> {
        use common::schema::share_offers::dsl::*;
        log::debug!(
            "Removing share offer {} for user {}",
            offer_id,
            recipient_id
        );
        let db_conn = self.get_db_conn()?;

        let offer = share_offers
            .filter(id.eq(offer_id))
            .get_result::<ShareOffer>(&db_conn)?;

        if offer.user_id != recipient_id {
            log::error!(
                "User {} trying to delete offer {} belonging to {}",
                recipient_id,
                offer_id,
                offer.user_id
            );
            return Err(RegistryError::Unauthorized);
        }

        diesel::delete(share_offers)
            .filter(id.eq(offer_id))
            .execute(&db_conn)?;

        Ok(())
    }

    fn get_db_conn(
        &self,
    ) -> Result<
        diesel::r2d2::PooledConnection<diesel::r2d2::ConnectionManager<diesel::PgConnection>>,
        RegistryError,
    > {
        self.db_pool.get().map_err(|e| {
            log::debug!("Unable to get DB connection from pool: {}", e);
            RegistryError::InternalError
        })
    }

    fn get_user_info(&self, query_username: &str) -> Result<UserInfo, RegistryError> {
        use common::schema::users::dsl::*;

        log::debug!("Getting id and public key for user {}", query_username);

        let db_conn = self.get_db_conn()?;

        let user_info = users
            .filter(username.eq(query_username))
            .select((id, enc_public_key))
            .get_result::<UserInfo>(&db_conn)
            .map_err(|e| {
                log::debug!("Unable to load user {}: {}", query_username, e);
                RegistryError::DieselError(e)
            })?;

        log::debug!(
            "Public key for user {} (id={}) is {:?}",
            query_username,
            user_info.id,
            user_info.enc_public_key
        );

        Ok(user_info)
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct FileInfo {
    pub owner_id: i32,
    pub data: Vec<u8>,
}
impl FileInfo {
    pub fn to_encrypted(&self, access_key: &[u8]) -> Result<EncryptedData, RegistryError> {
        // Serializing EncryptedData struct to vec
        let file_info_bytes = self.to_vec()?;

        let key = secretbox::Key::from_slice(access_key).ok_or_else(|| {
            log::error!("unable to parse acess key");
            RegistryError::InternalError
        })?;

        // Encrypting data with access key
        let nonce = secretbox::gen_nonce();
        let encrypted_data = secretbox::seal(&file_info_bytes, &nonce, &key);
        let encrypted_file = EncryptedData {
            encrypted_data,
            nonce: nonce.as_ref().to_vec(),
        };

        Ok(encrypted_file)
    }

    pub fn from_encrypted_file(data: &[u8], access_key: &[u8]) -> Result<FileInfo, RegistryError> {
        // Parsing access key and data
        let key = secretbox::Key::from_slice(access_key).ok_or_else(|| {
            log::error!("unable to parse acess key");
            RegistryError::InternalError
        })?;
        let encrypted_file = EncryptedData::from_slice(data)?;
        let nonce = secretbox::Nonce::from_slice(&encrypted_file.nonce).ok_or_else(|| {
            log::error!("unable to parse Nonce");
            RegistryError::InternalError
        })?;

        // Decrypt file with access key
        let info = secretbox::open(&encrypted_file.encrypted_data, &nonce, &key).map_err(|_| {
            log::error!("unable to decrypt file info");
            RegistryError::InternalError
        })?;

        FileInfo::from_slice(&info)
    }

    pub fn from_slice(data: &[u8]) -> Result<FileInfo, RegistryError> {
        rmp_serde::from_read_ref(data).map_err(|e| {
            log::error!("unable to parse file info: {}", e);
            RegistryError::InternalError
        })
    }

    pub fn to_vec(&self) -> Result<Vec<u8>, RegistryError> {
        rmp_serde::to_vec(self).map_err(|e| {
            log::error!("unable to parse serialize file info: {}", e);
            RegistryError::InternalError
        })
    }
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct OuterShareOffer {
    pub sender: String,
    pub details: Vec<u8>,
}
