table! {
    file_lists (user_id) {
        user_id -> Int4,
        file_list -> Bytea,
        version_tag -> Int8,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::db::File_state;

    files (id) {
        id -> Int4,
        file_data -> Bytea,
        state -> File_state,
    }
}

table! {
    masterkeys (id) {
        id -> Int4,
        master_key -> Bytea,
    }
}

table! {
    share_offers (id) {
        id -> Int4,
        user_id -> Int4,
        share_offer -> Bytea,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        salt -> Bytea,
        enc_login_secrets -> Bytea,
        enc_public_key -> Bytea,
    }
}

allow_tables_to_appear_in_same_query!(file_lists, files, masterkeys, share_offers, users,);
