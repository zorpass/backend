use std::sync::RwLock;

use anyhow::{Error, Result};

use dashmap::DashMap;
use uuid::Uuid;

#[derive(Default)]
pub struct SessionManager<T: Clone> {
    sessions_a: DashMap<uuid::Bytes, T>,
    sessions_b: DashMap<uuid::Bytes, T>,
    current: RwLock<bool>,
}
impl<T: Clone> SessionManager<T> {
    pub fn new() -> SessionManager<T> {
        SessionManager {
            sessions_a: DashMap::new(),
            sessions_b: DashMap::new(),
            current: RwLock::new(true),
        }
    }

    pub fn new_session(&self, item: T) -> Result<Vec<u8>> {
        let mut uuid_bytes = *Uuid::new_v4().as_bytes();

        // Insert in current session and encode it in uuid's lsb
        match self.current.read() {
            Ok(current) => {
                if *current {
                    uuid_bytes[0] &= 0xFE; // Set lsb to 0
                    self.sessions_a.insert(uuid_bytes, item);
                } else {
                    uuid_bytes[0] |= 0x01; // Set lsb to 1
                    self.sessions_b.insert(uuid_bytes, item);
                };
                Ok(*current)
            }
            Err(_) => Err(Error::msg("Unable to lock current session indicator")),
        }?;

        Ok(uuid_bytes.to_vec())
    }

    pub fn get(&self, id: &[u8]) -> Option<T> {
        match self.current.read() {
            Ok(_) => {
                let session = if id[0] & 0x01 == 0 {
                    self.sessions_a.get(id)
                } else {
                    self.sessions_b.get(id)
                };
                Some((*session?).clone())
            }
            Err(_) => {
                log::error!("Unable to lock current session indicator");
                None
            }
        }
    }

    pub fn swap_buffer(&mut self) {
        match self.current.write() {
            Ok(mut current) => {
                if *current {
                    self.sessions_b.clear();
                } else {
                    self.sessions_a.clear();
                };
                *current = !*current;
            }
            Err(_) => {
                log::error!("Unable to lock current session indicator");
            }
        };
    }
}
