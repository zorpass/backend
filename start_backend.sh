#!/usr/bin/env bash

INSTALL_PATH="/app"
ACCOUNT_SERVICE="${INSTALL_PATH}/account-server"
AUTHENTICATION_SERVICE="${INSTALL_PATH}/authentication-server"
MASTERKEYS_SERVICE="${INSTALL_PATH}/masterkeys-server"
REGISTRY_SERVICE="${INSTALL_PATH}/registry-server"

case $ZORPASS_SERVICE in
    account)
        exec $ACCOUNT_SERVICE
    ;;
    authentication)
        exec $AUTHENTICATION_SERVICE
    ;;
    masterkeys)
        exec $MASTERKEYS_SERVICE
    ;;
    registry)
        exec $REGISTRY_SERVICE
    ;;
    *)
        echo "Usage: set env ZORPASS_SERVICE to one of:"
        echo -e "\taccount"
        echo -e "\tauthentication"
        echo -e "\tmasterkeys"
        echo -e "\tregistry"
    ;;
esac